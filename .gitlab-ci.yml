# run the pipeline only on MRs, tags, and default branch
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

default:
  image: golang:1.18

stages:
  - documentation
  - test
  - release

variables:
  SAST_EXCLUDED_ANALYZERS: "bandit,gosec,eslint"

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - project: 'gitlab-org/quality/pipeline-common'
    file: '/ci/danger-review.yml'  # danger-review job below

# From: https://docs.gitlab.com/ee/ci/caching/#cache-go-dependencies
.go-cache:
  variables:
    GOPATH: $CI_PROJECT_DIR/.go
    GOLANGCI_LINT_CACHE: $CI_PROJECT_DIR/.golangci-lint
  before_script:
    - mkdir -p .go .golangci-lint
  cache:
    paths:
      - .go/pkg/mod/
      - .golangci-lint/

.documentation:
  stage: documentation
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

check_docs_update:
  extends: .documentation
  script:
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME && git checkout $CI_MERGE_REQUEST_TARGET_BRANCH_NAME && git checkout $CI_COMMIT_SHA
    - go run cmd/gen-docs/docs.go
    - |-
        git status
        if [[ $(git add -A --dry-run) ]]; then
          echo '✖ ERROR: Documentation changes detected!';
          echo '✖ These changes require a documentation update. To regenerate the docs, read https://gitlab.com/gitlab-org/cli/-/tree/main/docs#generating-the-docs.';
          exit 1;
        else
          echo '✔ No documentation updates detected.';
          exit 0;
        fi

check_docs_markdown:
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-markdown:alpine-3.16-vale-2.20.1-markdownlint-0.32.2
  extends: .documentation
  script:
    # Lint prose
    - vale --minAlertLevel error docs README.md
    # Lint Markdown
    - markdownlint --config .markdownlint.yml 'docs/**/*.md' README.md

lint_commit:
  stage: test
  image: node:16-slim
  script: 
    - apt-get update && apt-get install -y git
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME && git checkout $CI_MERGE_REQUEST_TARGET_BRANCH_NAME && git checkout $CI_COMMIT_SHA
    - cd scripts/commit-lint && npm ci
    - node lint.js
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_PROJECT_VISIBILITY == "public"' # lint.js script makes an API call without authentication
      when: always

lint:
  extends: .go-cache
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
  stage: test
  script:
    - golangci-lint --version
    # Use default .golangci.yml file from the image if one is not present in the project root.
    - '[ -e .golangci.yml ] || cp /golangci/.golangci.yml .'
    # Write the code coverage report to gl-code-quality-report.json
    # remove `--issues-exit-code 0` or set to non-zero to fail the job if linting issues are detected
    - golangci-lint run --out-format colored-line-number:stdout,code-climate:gl-code-quality-report.json
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json

code_navigation:
  stage: test
  image: golang:latest
  allow_failure: true
  script:
    - go install github.com/sourcegraph/lsif-go/cmd/lsif-go@latest
    - lsif-go
  artifacts:
    reports:
      lsif: dump.lsif

run_tests:
  stage: test
  extends: .go-cache
  before_script:
    ## Adding private SSH key to the executor, more information: https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
    - chmod 600 ~/.ssh/known_hosts
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
  script:
    # `goreleaser` also uses GITLAB_TOKEN and so we need to distinguish between
    # the GITLAB_TOKEN_TEST with less privilege used for testing and the GITLAB_TOKEN_RELEASE token
    - GITLAB_TOKEN=$GITLAB_TOKEN_TEST make test
  coverage: '/coverage: \d+.\d+% of statements/'
  artifacts:
    reports:
      junit: coverage.xml

secret_detection:
  rules:
    - if: $SECRET_DETECTION_DISABLED
      when: never
    - if: '$CI_MERGE_REQUEST_IID'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: []
  dependencies: [] # Don't download artifacts, especially `./public/`

.release:
  stage: release
  image: docker:stable
  services:
    - docker:dind
  variables:
    # Disable shallow cloning so that goreleaser can diff between tags to
    # generate a changelog.
    GIT_DEPTH: 0
  dependencies: [] # Don't download artifacts, goreleaser fails if the repository folder is dirty

release_test:
  extends: .release
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script: |
    docker run --rm --privileged \
      -v $PWD:/go/src/gitlab.com/gitlab-org/cli \
      -w /go/src/gitlab.com/gitlab-org/cli \
      -v /var/run/docker.sock:/var/run/docker.sock \
      goreleaser/goreleaser release --snapshot --rm-dist

release:
  extends: .release
  rules:
    - if: $CI_COMMIT_TAG
  variables:
    DOCKER_REGISTRY: $CI_REGISTRY
    DOCKER_USERNAME: $CI_REGISTRY_USER
    DOCKER_PASSWORD: $CI_REGISTRY_PASSWORD
  script: |
    docker run --rm --privileged \
      -v $PWD:/go/src/gitlab.com/gitlab-org/cli \
      -w /go/src/gitlab.com/gitlab-org/cli \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -e DOCKER_USERNAME -e DOCKER_PASSWORD -e DOCKER_REGISTRY  \
      -e GITLAB_TOKEN=$GITLAB_TOKEN_RELEASE \
      goreleaser/goreleaser release
